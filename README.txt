The Collapse Text Wysiwyg module is a simple extension of the Collapse Text
module which allows integrating the collapsible text markup with Wysiwyg, Rich
Text Editors.

It mostly implements: hook_wysiwyg_wysiwyg_plugin($editor) and the rest is
pretty much JS related with editors.

At the moment, it only supports CKEditor, but could be extended in the future to
also support other Wysiwyg editors.

Pretty much, it allows adding a button in the Rich Text Editor toolbar to
provide a more user friendly user interface to insert collapsible text through
the markup and syntax defined by the collapse_text module.
[More info about the syntax at: Collapse Text]

Clicking on the "insert collapsible text" button in the wysiwyg toolbar opens an
editor's dialog box with separate fields for each of the customizable properties
of the Collapse Text module:
- collapsible text title
- collapsible text body
- optional classes

Installation and configuration:
-------------------------------
0 - Prerequisite:
a. Requires wysiwyg and collapse_text modules to be installed.
b. Requires CKEditor wysiwyg libraries to be installed.

1 - Download the module and simply put into your contributed modules folder:
[for example, your_drupal_path/sites/all/modules] and enable it from the modules
administration/management page.

2 - Configuration:
After successful installation you should be able to see a "configure" link on
the modules admin page, next to the "Collapse Text Wysiwyg" module.
click on this configure link, or directly browse to admin/config/content/wysiwyg
Home » Administration » Configuration » Content authoring

3 - Edit the CKEditor profiles which should enable this button to be displayed
in the toolbar. In the Edit settings, under the "Buttons and Plugins" settings,
a new option/checkbox should be displayed: "Insert Collapsible Text"
Check this setting to enable it, then save.

Contributions are welcome!!
---------------------------
Feel free to follow up in the issue queue for any contributions, for example,
support across other wysiwyg systems, such as TinyMCE, could be nice to add to
the module. Tests, feedback or comments in general are highly appreciated.

This module was sponsored by DAVYIN (http://www.davyin.com/).
